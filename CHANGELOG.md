# Changelog

## To do

When each version is released:
  - This log will be updated with up to date Added, Changed, Fixed, Removed for each version
  - This To do section should be used to announce future changes, especially where they will impact users due to breaking backward compatibility, or to address major open issues that could not be addressed in the most recent release.
  - The links from the version numbers currently assumes GitLab (which adds a hyphen, while GitHub does not); if anyone knows of a way to link to the tagged release that works across platforms, do let us know. Note: you have to `git tag <version>` and push the tag with `git push <version>`.

## [Unreleased]

### Added
  - etc.

## [0.0.1] - 2021-08-04

### Added
  - Cloned and adapted from python_package_template

[Unreleased]: /../../../
[0.0.1]: /../../../tags/0.0.1
