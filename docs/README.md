# Documentation

To get started with `mkdocs`, from a separate terminal tab/window:
```commandline
pip install mkdocs
mkdocs new .
mkdocs serve
```
